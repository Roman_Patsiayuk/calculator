package com.epam;

import com.epam.calculator.Calculator;
import com.epam.exceptions.DivideByZeroException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Roman
 */
public class Runner {

    private static int number;
    private static double firstNumber;
    private static double secondNumber;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        runCalculator();
    }

    public static void runCalculator() {
        Calculator math = new Calculator();
        boolean repeat = true;
        Runner runner = new Runner();
            while (repeat) {

                System.out.println("Please, enter number of menu:");
                System.out.println("0 - Exit");
                System.out.println("1 - Addition");
                System.out.println("2 - Subsctraction");
                System.out.println("3 - Multiplication");
                System.out.println("4 - Division");
                System.out.println("5 - Root");
                System.out.println("6 - Power");
                System.out.println("7 - Fibonacci");

                int action;
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
                    String str = bufferedReader.readLine();
                    action = Integer.parseInt(str);
                }catch (Exception e) {
                    e.printStackTrace();
                    System.err.println("Error: enter correct int value");
                    continue;
                }
                switch (action) {
                    case 0:
                        repeat = false;
                        System.out.println("Completed");
                        break;
                    case 1:
                        runner.enterNumbers();
                        System.out.println("Addition result = " + math.addition(firstNumber, secondNumber));
                        break;
                    case 2:
                        runner.enterNumbers();
                        System.out.println("Subsctraction result = " + math.subsctraction(firstNumber, secondNumber));
                        break;
                    case 3:
                        runner.enterNumbers();
                        System.out.println("Multiplication result = " + math.multiplication(firstNumber, secondNumber));
                        break;
                    case 4:
                        runner.enterNumbers();
                        try {
                            System.out.println("Division result = " + math.division(firstNumber, secondNumber));
                        } catch (DivideByZeroException e) {
                            System.out.println(e.getMessage());
                        }
                        break;
                    case 5:
                        runner.enterNumber();
                        try {
                            System.out.println("Root result = " + math.root(number));
                        } catch (IllegalArgumentException e) {
                            System.out.println(e.getMessage());
                        }
                        break;
                    case 6:
                        runner.enterNumbers();
                        try {
                            System.out.println("Power result = " + math.power(firstNumber, secondNumber));
                        } catch (IllegalArgumentException e) {
                            System.out.println(e.getMessage());
                        }
                        break;
                    case 7:
                        runner.enterNumber();
                        for (int i = 0; i <= number; i++) {
                            System.out.print(math.recursionFibonacci(i) + " ");
                        }
                        System.out.print("\n");
                        break;
                    default:
                        System.out.println("Incorrect value! Please, try again.");
                        break;
                }
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void enterNumbers() {
        System.out.print("Please enter first number: ");
        if (scanner.hasNextDouble()) {
            firstNumber = scanner.nextDouble();
        } else {
            throw new IllegalArgumentException("Not a number");
        }
        System.out.print("Please enter second number: ");
        if (scanner.hasNextDouble()) {
            secondNumber = scanner.nextDouble();
        } else {
            throw new IllegalArgumentException("Not a number");
        }
    }

    public void enterNumber() {
        System.out.print("Please enter integer number: ");
        if (scanner.hasNextInt()) {
            number = scanner.nextInt();
        } else {
            throw new IllegalArgumentException("Not integer number");
        }
    }
}
