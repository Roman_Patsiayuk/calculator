package com.epam.calculator;

import com.epam.exceptions.DivideByZeroException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman
 */
public class Calculator {

    public BigDecimal addition(double firstNumber, double secondNumber) {
        return BigDecimal.valueOf(firstNumber + secondNumber);
    }

    public BigDecimal subsctraction(double firstNumber, double secondNumber) {
        return BigDecimal.valueOf(firstNumber - secondNumber);
    }

    public BigDecimal multiplication(double firstNumber, double secondNumber) {
        return BigDecimal.valueOf(firstNumber * secondNumber);
    }

    public BigDecimal division(double firstNumber, double secondNumber) throws DivideByZeroException {
        if (secondNumber == 0) { //проверка, если 2число 0 выбрасываем исключение, о том что происходит деление на 0
            throw new DivideByZeroException("Divide by zero!");
        }
        return BigDecimal.valueOf(firstNumber / secondNumber);
    }

    public BigDecimal root(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("A negative number");
        }
        return BigDecimal.valueOf(Math.sqrt(number));
    }

    public BigDecimal power(double firstNumber, double secondNumber) {
        if (secondNumber == 0) {
            return BigDecimal.valueOf(1.0);
        } else if ((firstNumber == 0.0) && (secondNumber < 0)) {
            throw new IllegalArgumentException("Infinity");
        }
        return BigDecimal.valueOf(Math.pow(firstNumber, secondNumber));
    }

    public int recursionFibonacci(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("Negative number");
        } else if (number == 0)
            return 0;
        else if (number == 1 || number == 2)
            return 1;
        else
            return recursionFibonacci(number - 1) + recursionFibonacci(number - 2);

    }

    public List<Integer> fibonacciSequence(int number) {
        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i <= number; i++) {
            list.add(recursionFibonacci(i));
        }
        return list;
    }

    public int fibonacci(int number) {
        int a = 1;
        int b = 1;
        int fibonacciResult = 1;
        if (number < 0) {
            throw new IllegalArgumentException("Negative number");
        } else if (number == 0) {
            return 0;
        } else if (number == 1 || number == 2) {
            return 1;
        } else {
            for (int i = 3; i < number; i++) {
                fibonacciResult = a + b;
                a = b;
                b = fibonacciResult;
            }
        }
        return fibonacciResult;
    }
}

