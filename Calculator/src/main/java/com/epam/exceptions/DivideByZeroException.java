package com.epam.exceptions;

/**
 * Created by Roman
 */
public class DivideByZeroException extends Exception {

    private String msg;

    public DivideByZeroException(String msg) {
        this.msg = msg;
    }

    public String getMessage() {
        return "[DivideByZeroException]:+ " + msg;
    }
}
