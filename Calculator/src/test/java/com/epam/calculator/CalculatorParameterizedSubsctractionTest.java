package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedSubsctractionTest extends BaseTest {

    public CalculatorParameterizedSubsctractionTest(double firstNumber, double secondNumber, double result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})" )
    public static List<Object[]> subsctractionDataTest() {
        return Arrays.asList(new Object[][]{
                {1, 2,-1},
                {-2.3,3.3,-5.6},
                {-2,-7,5},
                {-2,0,-2},
                {0,0,0}
        });
    }

    @Test()
    public void subsctractionTest() {
        Assert.assertEquals(BigDecimal.valueOf(result), calculator.subsctraction(firstNumber,secondNumber));
    }

}
