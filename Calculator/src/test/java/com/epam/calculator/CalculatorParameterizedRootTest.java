package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedRootTest extends BaseTest {

    public CalculatorParameterizedRootTest(int number, double result) {
        this.number = number;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})")
    public static List<Object[]> rootDataTest() {
        return Arrays.asList(new Object[][]{
                {4, 2},
                {16, 4},
                {0, 0},
                {1, 1},
                {100, 10}
        });
    }

    @Test()
    public void rootTest() {
        Assert.assertEquals(BigDecimal.valueOf(result), calculator.root(number));
    }

}
