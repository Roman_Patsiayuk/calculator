package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.runners.Parameterized.*;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedAdditionTest extends BaseTest {

    public CalculatorParameterizedAdditionTest(double firstNumber, double secondNumber, double result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    @Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})" )
    public static List<Object[]> additionData() {
        return Arrays.asList(new Object[][]{
                {1, 2,3},
                {2.3,3.3,5.6},
                {-2,-7,-9},
                {-2,12,10},
                {0,0,0}
        });
    }

    @Test
    public void additionTest() {
        Assert.assertEquals(BigDecimal.valueOf(result), calculator.addition(firstNumber,secondNumber));
    }

}
