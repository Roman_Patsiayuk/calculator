package com.epam.calculator;

import com.epam.exceptions.DivideByZeroException;
import org.junit.Test;

/**
 * Created by Roman
 */

public class CalculatorNegativeTest extends BaseTest {

    @Test(expected = DivideByZeroException.class)
    public void divisionTest() throws DivideByZeroException {
        calculator.division(2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rootTest() {
        calculator.root(-7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void powerTest() {
        calculator.power(0, -3);
    }

    @Test/*(expected = IllegalArgumentException.class)*/
    public void fibonacciTest() {
        calculator.recursionFibonacci(-3);
    }
}
