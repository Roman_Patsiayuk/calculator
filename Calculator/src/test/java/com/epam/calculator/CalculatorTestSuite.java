package com.epam.calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Roman
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorParameterizedSubsctractionTest.class, CalculatorParameterizedRootTest.class,CalculatorParameterizedPowerTest.class,
        CalculatorParameterizedMultiplicationTest.class,CalculatorParameterizedDivisionTest.class,
        CalculatorParameterizedAdditionTest.class,CalculatorNegativeTest.class,CalculatorFibonacciHamcrestTest.class})
public class CalculatorTestSuite {
}
