package com.epam.calculator;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

/**
 * Created by Roman
 */

public class CalculatorFibonacciHamcrestTest extends BaseTest {

    @Test
    public void fibonacciNumberValueTest() {
        assertThat(calculator.recursionFibonacci(0),is(0));
        assertThat(calculator.recursionFibonacci(1),is(1));
        assertThat(calculator.recursionFibonacci(2),is(1));
        assertThat(calculator.recursionFibonacci(3),is(2));
        assertThat(calculator.recursionFibonacci(4),is(3));
    }

    @Test
    public void fibonacciListSizeTest() {
        assertThat(calculator.fibonacciSequence(6), hasSize(7));
    }

    @Test
    public void fibonacciItemTest() {
        assertThat(calculator.fibonacciSequence(8), hasItem(3));
    }

    @Test
    public void fibonacciItemsTest() {
        assertThat(calculator.fibonacciSequence(9), hasItems(0,1,1,2,3,5));
    }

    @Test
    public void fibonacciSequenceTest() {
        assertThat(calculator.fibonacciSequence(4), contains(0,1,1,2,3));
    }
}
