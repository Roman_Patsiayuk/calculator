package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedPowerTest extends BaseTest {

    public CalculatorParameterizedPowerTest(double firstNumber, double secondNumber, double result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})" )
    public static List<Object[]> powerDataTest() {
        return Arrays.asList(new Object[][]{
                {1, 1,1},
                {0,10,0},
                {2,2,4},
                {-2,2,4},
                {1.2,2,1.44},
                {4,2,16},
                {4,-2,0.0625},
                {10,0,1}
        });
    }

    @Test
    public void powerTest() {
        Assert.assertEquals(BigDecimal.valueOf(result), calculator.power(firstNumber,secondNumber));
    }

}
