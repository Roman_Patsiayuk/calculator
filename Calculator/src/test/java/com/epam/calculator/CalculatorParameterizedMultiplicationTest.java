package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedMultiplicationTest extends BaseTest {

    public CalculatorParameterizedMultiplicationTest(double firstNumber, double secondNumber, double result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})" )
    public static List<Object[]> multiplicationDataTest() {
        return Arrays.asList(new Object[][]{
                {1, 1,1},
                {0,10,0},
                {0,-4,0},
                {-7,0,0},
                {1.2,2,2.4},
                {4,2,8},
                {4,-2,-8},
                {5.2,-5,-26}
        });
    }

    @Test()
    public void multiplicationTest() {
        Assert.assertEquals(BigDecimal.valueOf(result), calculator.multiplication(firstNumber,secondNumber));
    }

}
