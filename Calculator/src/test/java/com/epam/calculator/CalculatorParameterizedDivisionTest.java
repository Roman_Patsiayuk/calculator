package com.epam.calculator;

import com.epam.exceptions.DivideByZeroException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Roman
 */

@RunWith(Parameterized.class)
public class CalculatorParameterizedDivisionTest extends BaseTest {

    public CalculatorParameterizedDivisionTest(double firstNumber, double secondNumber, double result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: firstNumber({0}) and secondNumber({1})" )
    public static List<Object[]> divisionDataTest() {
        return Arrays.asList(new Object[][]{
                {1, 1,1},
                {0,10,0},
                {0,-4,0},
                {2,2,1},
                {-2,2,-1},
                {1.2,2,0.6},
                {4,2,2},
                {4,-2,-2},
                {5.2,-5,-1.04}
        });
    }

    @Test()
    public void divisionTest() {
        try {
            Assert.assertEquals(BigDecimal.valueOf(result), calculator.division(firstNumber,secondNumber));
        } catch (DivideByZeroException e) {
            e.printStackTrace();
        }
    }

}
