package com.epam.calculator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

/**
 * Created by Roman
 */
public abstract class BaseTest extends Assert {

    Calculator calculator;
    protected int number;
    protected double firstNumber;
    protected double secondNumber;
    protected double result;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }

    @After
    public void tearDown() {
        calculator = null;
    }
}
